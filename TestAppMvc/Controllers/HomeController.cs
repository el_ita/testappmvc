﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestAppMvc.Models;
using TestAppMvc.Util;

namespace TestAppMvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly BookContext _db = new BookContext();

        public ActionResult Index()
        {
            IEnumerable<Book> books = _db.Books;

            ViewBag.Books = books;

            return View();
        }

        [HttpGet]
        public ActionResult Buy(int id) 
        {
            ViewBag.BookId = id;

            return View();
        }

        [HttpPost]
        public string Buy(Purchase purchase)
        {
            purchase.Date = DateTime.Now;

            _db.Purchases.Add(purchase);

            _db.SaveChanges();

            return $"Спасибо, {purchase.Person}, за покупку!";
        }

        public ActionResult GetHtml()
        {
            return new HtmlResult("<h2>Привет мир!</h2>");
        }

        public ActionResult GetFile()
        {
            return new FilePathResult("~/App_Data/ticket.pdf", "application/pdf");
        }

        public ActionResult Redirect()
        {
            return new RedirectResult("http://ya.ru");
        }

        public ActionResult NotFound()
        {
            return new HttpNotFoundResult();
        }

        public ActionResult TestView()
        {
            return View("Index");
        }
    }
}